README FILE
=============================================================

Este repositorio contiene los archivos del código fuente en MATLAB para el proceso de compresión y reconstrucción con la técnica de CS utilizando los algoritmos IHT y OMP, y el enfoque de bloques sobrepuestos OBB-CS.

(+) Carpeta BlockLapped-CS:

Contiene los archivos asociados con el enfoque OBB-CS y un archivo principal que muestra un ejemplo de aplicación del enfoque, el cual puede ser ejecutado variando varios parámetros.

(+) Carpeta Compress-Recovery CS

Contiene los algoritmos OMP e IHT, junto con ejemplo de utilización de los algoritmos con imágenes de pequeñas dimensiones cuando se aplica la técnica de Compressive Sensing (estos fueron los archivos utilizados en las primeras pruebas).


=============================================================
Contacto: irenelizeth@gmail.com