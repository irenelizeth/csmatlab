function fii = generatesbhe(block_size, rate, k)
% This function generates the random projection matrix SBHE
% Phi for the given block size, rate and dimension k of the 
% hadamard matrix.

% SBHE Phi is returned as a M x N matrix, where N = block_size *
% block_size, and M = round(rate * N).

N = block_size*block_size;

tic
%% Block Diagonal Matrix
% Construct a Hadarmard Matrix B=32 through Konecker product
% Sylvester Matrix of order 2^k: kronecker product of k copies of H
% Sylvester Hadamard Matrix of order B = 2^k
% k = 5, B=32
k=3;

WB = sylvester_matrix(k);

%% Block Diagonal Matrix of hadamard Matrix WB 
I=eye(N/(2^k));
W = kron(sparse(I),sparse(WB));

%% Compute a simple pixel level scramble operator
% figure('Name','prueba');
[P,Ap] = compute_scramble_operator(N,1);
res = W*P;

%% Calculing fi-sub-f
% fi = Q*W*P;
% M ~ K = 5020 (Q sub M) suposse K =5020
% M = 5000;
% M = 10000;
% M = 15000;
% M = 20000;
% M = 25000;

% Generating a uniform random selector of nB rows
M = round(rate * N) % nB in Lu Gan paper
Q = unidrnd(N,1,M);
Q = sort(Q);
fii = sparse(M,N);

% Calculing measurement matrix fi
for i=1:1:length(Q)
    fii(i,:)= res(Q(i),:);
end

save('fiisbhe.mat','fii');
toc
