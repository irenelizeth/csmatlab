function X = lappedblock(a, block, over)

   [ma,na] = size(a);
    m = block(1); n = block(2);
    
    %step increase based on overlapping factor o
    step = m - over;
    
    if any([ma na] < [m n]) % if neighborhood is larger than image
        b = zeros(m*n,0);
        return
    end
    
    % Create Hankel-like indexing sub matrix.
    mc = block(1); nc = ma-m+1; nn = na-n+1;
    
    % number of blocks, q
    q = (-na+over)/(over-m);
    
    % creating basic matrix of column index
    t = zeros(mc, q);
    t(:,1) = (1:mc)';

    for i=1:1:q-1
        t(:,i+1) = t(:,i) + mc - over;
    end    
       
    % tt = zeros(mc*n,nc-over);
    tt = zeros(mc*n,q);

    rows = 1:mc;
   
    for i=0:n-1,
        tt(i*mc+rows,:) = t+ma*i;
    end
    
    ttt = zeros(mc*n,q*q);
    cols = 1:q;

    for j=0:q-1,
        ttt(:,j*q+cols) = tt+ma*step*j;
    end
    
    X = a(ttt);


