function [xs,te] = omp_alg(y, A, k, err, niter, verbose)

%Orthogonal Matching Pursuit Algorithm
% Input
%   Mandatory:
%               y   	 Observation vector to be decomposed,[mx1]
%               A   	 An mxn matrix (m must be dimension of y)
%               k        sparsity of the signal x
%   Optional:
%               err      allowed approximation error
%               niter    number of iterations, should be the value for the
%                        sparsity of the signal
%               verbose  print info at each iteration   
%   Output:
%               s        signal approximation
%               res      residual
%               te       elapsed time
%

if nargin < 4
     err = 1e-5;
end

% initial residual, mx1
r = y;

%dimension of the vextor x
n = size(A,2);
m = size(y,1);

% sparse vector
xs = zeros(size(A,2),1);

% % indices better of the projection: initial support, hold the indexes of the better projections
 Sup = [];

%possible indixes
% I = 1:size(A,2);

tic

%% variables for analysis

% vector to save the number of selected columns in each iteration, l0-norm
sv = zeros(niter,1);
% residual of each iteration
rv = zeros(niter,1);

%% OMP algorithm
for i=1:niter
    % current error: gradient vector, c
    c = A'*r;
    
    if i>1
        % don't take into account the previously selected columns
        c(Sup)=0;
        % find the max index j
        [maxV, maxj] = max(abs(c));
    else
        % find the max index j
        [maxV, maxj] = max(abs(c));
    end
    
    %i
    %maxj
    
    % update the support with the indexes
    Sup = [Sup' maxj']';
        
    %%remove the index from the set of possible indexes
    %update the matrix with the columns of the indexes in S
    Psi = A(:,Sup);
    
    % update the solution xs, projection of y on Psi
    % moore pseudo inverse: ((Psi'*Psi)^-1)*Psi'
    xs(Sup)=((Psi'*Psi)^-1)*Psi'*y;
    % xs(Sup) =Psi\y;
    
    % save the residual of this iteration i
    r = y - A*xs;
    
    % metrics for analysis
    sv(i)= maxj;
    rv(i)=norm(r);

    if verbose==1
        fprintf('Iteration %u, selected column: %i, l2-r: %f \n',i,maxj,rv(i));
    end 
    
    if(norm(r) < err)
        break;
    end
end

te = toc;