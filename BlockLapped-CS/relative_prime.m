% calculate if two numbes are relative prime
% - A, B numbers
% res is 1 if the two numbers are relatively prime

function res = relative_prime(A,B)

% the number A is relative prime with B
res = 1;

fA = factor(A);
fB = factor(B);
r=0;
for i=1:1:length(fA)
    r = r + length(find(fB==fA(i)));
end

if(r>=1)
    res = 0;
end