function F = rearrangeBlocks(m, n, X, over, blockSize, q)

% m, n,        size of the original image, assume square image
% X,           matriz with the overlapped blocks of the image
% over,        overlaping factor
% blockSize,   size of each block
% q,           number of blocks

    b =  blockSize;
    F = zeros(m,n);                         % New image
    
    %     block = zeros(b, b);              % ith Block of the image
    %     C = zeros(over, b);                     % C, shared part at the botton side
    %     D = zeros(b, over);                     % D, shared part at the right side
    %     B = zeros(b, over);                     % B, shared part at the left side
    %     A = zeros(over, b);                     % A, shared part at the upper side
    
    % Reorganize the image with the sparse blocks:
    % Arrange the blocks of the new Image
     
    flagh=0;       % indicates if the block next to the right side is take into account
    flagv=0;       % indicates if the block next under the current block is take into account
     
     for j=1:size(X,2)
            
            % Recognize which tye of block
            
            rowb = mod(j,q);
            colb = ceil(j/q);

            if  rowb==0
                rowb=q;
            end
            
            if rowb==1
                % Get the unshared area of the block m.n                
                % consider corners and upper sides
                if colb==1 
                    %left corner has to consider D,C,A,B,Z
                    limc = blockSize-over;
                    limr = blockSize-over;
                    
                    usc = 1;                                % index row starting unshared area
                    usr = 1;                                % index col starting unshared area
                    
                    flagv=1;
                    flagh=1;
                    
                    mb = 1: limr;
                    nb = 1: limc;
                    
                elseif colb < q && colb > 1
                    %middle blocks consider D,C,A,B,Z
                    limc = blockSize-2*over;
                    limr = blockSize-over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = 1;                                % index col starting unshared area
                    
                    flagv=1;
                    flagh=1;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                    
                elseif colb==q
                    %right corner has to consider C,A
                    limc = blockSize-over;
                    limr = blockSize-over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = 1;                                % index col starting unshared area
                    
                    flagv=1;
                    flagh=0;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                    
                end
                
            % consider left side, right side and center blocks
            elseif rowb < q
                if colb==1 
                    %left corner has to consider D,C,A,B,Z
                    limc = blockSize-over;
                    limr = blockSize-over;
                    
                    usc = 1;                                % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=1;
                    flagh=1;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-over-1;
                    nb = ni : ni+limc-1;
                    
                elseif colb < q && colb > 1
                  limc = blockSize-2*over;
                  limr = blockSize-2*over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=1;
                    flagh=1;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                    
                elseif colb==q
                    limc = blockSize-over;
                    limr = blockSize-2*over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=1;
                    flagh=0;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                    
                end
                
            % consider corners blocks and the bottom side
            elseif rowb==q
                 if colb==1 
                    %left corner has to consider D,B
                    limc = blockSize-over;
                    limr = blockSize-over;
                    
                    usc = 1;                                % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=0;
                    flagh=1;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                    
                elseif colb < q && colb > 1
                    limc = blockSize-2*over;
                    limr = blockSize-over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=0;
                    flagh=1;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                elseif colb==q
                    limc = blockSize-over;
                    limr = blockSize-over;
                    
                    usc = over+1;                           % index row starting unshared area
                    usr = over+1;                           % index col starting unshared area
                    
                    flagv=0;
                    flagh=0;
                    
                    mi = getIndex(rowb, b, over);           % get the index of row where to put the data in F
                    ni = getIndex(colb, b, over);           % get the index of Column where to put the data in F
                    mb = mi : mi+limr-1;
                    nb = ni : ni+limc-1;
                 end
            end
               
                
            % Structure the blocks into a new image F
            
            if flagv==1 && flagh==1     % Consider next blocks at both sides
                
                block  = reshape(X(:,j),blockSize, blockSize);          % block m,n
                blockn = reshape(X(:,j+q),blockSize, blockSize);        % block m,n+1
                blockm = reshape(X(:,j+1),blockSize, blockSize);        % block m+1,n
                blockmn = reshape(X(:,j+q+1),blockSize, blockSize);     % block m+1,n+1
                
                 % Get the shared parts of the blocks involved
                
                if colb==1 && rowb==1
                    
                    C = block(limr+1:b,1:b);                    % C = block(limr+1:b,1:b-over);
                    D = block(1:b,limc+1:b);                    % D = block(1:b-over,limc+1:b);

                    B = blockn(1:b,1:over);                     % B = blockn(over+1:b,1:over);
                    A = blockm(1:over,1:b);                     % A = blockm(1:over,over+1:b);

                    % Take the average of the shared pixels

                    Cn = (C+A)/2;                               % right column
                    Dn = (D+B)/2;                               % row at the bottom
                    % lower right corner
                    Zn = (block(limr+1:b, limc+1:b) + blockm(1:over, limc+1:b) + blockn(limr+1:b,1:over) + blockmn(1:over,1:over))/4;

                    % Put the recovered block into the new image F

                    % location in F is different for each block
                    F(mb,nb) = block(usr:limr,usc:limc); % assigning E, unshared area of the block
                    
                    F(limr+1:b,1:b) = Cn;                       % bottom row
                    F(1:b,limc+1:b) = Dn;  
                    F(limr+1:b, limc+1:b) = Zn;                 %right bottom corner
                
                elseif colb>1 && rowb==1
                    
                    C = block(limr+1:b,over+1:b);               % C = block(limr+1:b,1:b-over);
                    D = block(1:b,limc+over+1:b);               % D = block(1:b-over,limc+1:b);

                    B = blockn(1:b,1:over);                     % B = blockn(over+1:b,1:over);
                    A = blockm(1:over,over+1:b);                % A = blockm(1:over,over+1:b);

                    % Take the average of the shared pixels

                    Cn = (C+A)/2;                               % right column
                    Dn = (D+B)/2;                               % row at the bottom
                    % lower right corner
                    Zn = (block(limr+1:b, limc+over+1:b) + blockm(1:over, limc+over+1:b) + blockn(limr+1:b,1:over) + blockmn(1:over,1:over))/4;

                    % Put the recovered block into the new image F

                    % location in F is different for each block
                    F(mb,nb) = block(usr:limr,usc:limc+over);                   % assigning E, unshared area of the block
                    
                    mnb = min(nb):max(nb)+over;
                    F(max(mb)+1:max(mb)+over,mnb) = Cn;
                    mmb = min(mb):max(mb)+over;     
                    F(mmb,max(nb)+1:max(nb)+over) = Dn;                         % Change te dimension of D 
                    F(max(mb)+1:max(mb)+over, max(nb)+1:max(nb)+over) = Zn;     %right bottom corner
                    
                elseif rowb>1 && colb==1
                    
                    C = block(limr+1:b,1:b);                    % C = block(limr+1:b,1:b-over);
                    D = block(1+over:b,limc+1:b);               % D = block(1:b-over,limc+1:b);

                    B = blockn(1+over:b,1:over);                % B = blockn(over+1:b,1:over);
                    A = blockm(1:over,1:b);                     % A = blockm(1:over,over+1:b);

                    % Take the average of the shared pixels

                    Cn = (C+A)/2;                               % right column
                    Dn = (D+B)/2;                               % row at the bottom
                    % lower right corner
                    Zn = (block(limr+1:b, limc+1:b) + blockm(1:over, limc+1:b) + blockn(limr+1:b,1:over) + blockmn(1:over,1:over))/4;

                    % Put the recovered block into the new image F

                    % location in F is different for each block
                    F(mb,nb) = block(usr:limr,usc:limc);        % assigning E, unshared area of the block
                    
                    mnb = min(nb):max(nb)+over;
                    F(max(mb)+1:max(mb)+over,mnb) = Cn;
                    mmb = min(mb):max(mb)+over;       
                    F(mmb,max(nb)+1:max(nb)+over) = Dn;         % Change te dimension of D 
                    F(max(mb)+1:max(mb)+over, max(nb)+1:max(nb)+over) = Zn; %right bottom corner
                else
                    
                    C = block(limr+over+1:b,over+1:b);          % C = block(limr+1:b,1:b-over);
                    D = block(1+over:b,limc+over+1:b);          % D = block(1:b-over,limc+1:b);

                    B = blockn(1+over:b,1:over);                % B = blockn(over+1:b,1:over);
                    A = blockm(1:over,over+1:b);                % A = blockm(1:over,over+1:b);

                    % Take the average of the shared pixels

                    Cn = (C+A)/2;                               % right column
                    Dn = (D+B)/2;                               % row at the bottom
                    % lower right corner
                    Zn = (block(limr+over+1:b, limc+over+1:b) + blockm(1:over, limc+over+1:b) + blockn(limr+over+1:b,1:over) + blockmn(1:over,1:over))/4;

                    % Put the recovered block into the new image F

                    % location in F is different for each block
                    F(mb,nb) = block(usr:limr+over,usc:limc+over); % assigning E, unshared area of the block
                    
                    mnb = min(nb):max(nb)+over;
                    F(max(mb)+1:max(mb)+over,mnb) = Cn;
                    mmb = min(mb):max(mb)+over;       
                    F(mmb,max(nb)+1:max(nb)+over) = Dn; % Change te dimension of D 
                    F(max(mb)+1:max(mb)+over, max(nb)+1:max(nb)+over) = Zn; %right bottom corner
                end
               
            elseif flagv==0 && flagh==0                          % no more blocks, no averages
             
                block  = reshape(X(:,j),blockSize, blockSize);   % block m,n
                F(mb,nb) = block(usr:b,usc:b);                   % assigning E, unshared area of the block
            
            elseif flagv==0 && flagh==1                          % last row, average only D with B of next block
                
                block  = reshape(X(:,j),blockSize, blockSize);   % block m,n
                blockn = reshape(X(:,j+q),blockSize, blockSize); % block m,n+1
               
                % Get shared parts of the blocks involved
            
                D = block(1+over:b,b-over+1:b);
                B = blockn(1+over:b,1:over);       
                
                %D = block(1:b,limc+1:b);                    % D = block(1:b-over,limc+1:b);
                %B = blockn(1:b,1:over);                     % B = blockn(over+1:b,1:over);

                % Take the average of the shared pixels
                Dn = (D+B)/2;                               % row at the bottom
               
                % Put the recovered block into the new image F
                % location in F is different for each block
                F(mb,nb) = block(usr:b,usc:usc+limc-1);        % assigning E, unshared area of the block
                F(mb,max(nb)+1:max(nb)+over) = Dn;          % Change te dimension of D 
                                
            else                            % last column, average only C with A of next block

                block  = reshape(X(:,j),blockSize, blockSize);   % block m,n
                blockm = reshape(X(:,j+1),blockSize, blockSize); % block m+1,n
              
                % Get the shared parts of the blocks involved
                C = block(b-over+1:b,over+1:b); % C = block(limr+1:b,1:b-over);
                A = blockm(1:over,over+1:b);  % A = blockm(1:over,over+1:b);

                % Take the average of the shared pixels
                Cn = (C+A)/2; % right column
                
                % Put the recovered block into the new image F
                % location in F is different for each block
                F(mb,nb) = block(usr:b-over,usc:b); % assigning E, unshared area of the block
                
                F(max(mb)+1:max(mb)+over,nb) = Cn;  % bottom row
            end
      end
    
        