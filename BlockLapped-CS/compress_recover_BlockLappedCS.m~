%{ Example of Block Lapped Algoritm for Image Compression/Reconstruction
% with CS. For each image dimension select the block size for the image 
% blocks and the proper name for the files saved on the disk according 
% to the dimensions of the image and the parameters selected for the the
% compression%}

% Example for image Lenna [512x512] pixels

% Note: this example is illustrative only, the maximum dimension used on the
% mobile device was [128x128] pixels and using blocks of 32 pixels
% due to memory constraints.

clear all; clc;
close all;

%% Compression - Recovery - Lapped block-based CS

    %     filename = 'images/lenaeye';
    %     original = [filename '.pgm'];

    %     filename = 'images/lena128';
    %     filename = 'images/lena256';
    %     original = [filename '.jpg'];

    filename = 'images/lena512';
    original = [filename '.pgm'];

    originalImage = double(imread(original));
            
    % imshow(originalImage,[min(originalImage(:)) max(originalImage(:))])

    dim = size(originalImage, 1);

    %% First) Get overlapped blocks from the image

    [m n] = size(originalImage);

    blockSize =  n/16;              % block sizeq

    q = n/blockSize + 1;            % number of blocks, q
    over = (q*blockSize-n)/(q-1);   % overlapping factor, over
    
    % Generate lapped blocks from the image
    X = lappedblock(originalImage, [blockSize blockSize], over);
   
    
    %% (Optional) save the blocks of the image
    dlmwrite('X-lena512-NoSparse-b32-o2.mat',X,';');
    
    %% (Optional) Read blocks
    % X = dlmread('X-lena512-NoSparse-b32-o2.mat',';');
    
    %% Get sparse blocks:
    
    x = zeros(blockSize*blockSize,1);
    
    % fixed sparsity (k) for all blocks of the image: sparsity ratio
    spar = 0.05;                     
    
    % number of measurements 
    mes = 4*spar;
    
    if blockSize < 64
        % Change wavelet (wt)'s dimension according with blockSize
        wt = MatrixW(blockSize, 1,1,1,0);
        w = wt';
        XS = zeros(blockSize*blockSize, q*q);
    elseif blockSize==64 
        % Loading representation matrix, Psi
        namM = strcat('MatrixWavelet/','w',num2str(blockSize),'.mat');
        load(namM);
        w = w64;
        namMt= ['MatrixWavelet/','wt',num2str(blockSize),'.mat'];
        load(namMt);
        wt=wt64;
    end
    
    %   Using sparsity level k fixed for each block
    for i=1:size(X,2)
        
        x = X(:,i);
        % set the sparsity level k
        k = round(size(x,1)*spar);
        % set the number of elements to convert to zero
        con = round((1-spar)*size(x,1));
        vecBlock = w*x;
        vecBlockSort = sortrows([abs(vecBlock),(1:length(vecBlock))'],1);
        
        % setting k sparsity
        vecBlock(vecBlockSort(1:con,2))=0;
        % getting the new sparse image in the original domain
        vec = wt*vecBlock;
        % sparse image in the original domain
        XSparse(:,i) = vec; 
        
        % matriz that holds the sparse blocks of the image
        XS(:,i) = vecBlock;

    end
        
    %% Save on disk sparse blocks in time domain -- Save on disk sparse blocks on the representation and time domains  
    % for i=1:size(XSparse,2)
    %   dirnameblocks = 'blocks/';
    %   nameFile =  strcat(dirnameblocks, num2str(m),'Oriblock-lenna','-',num2str(i),'.out');
    %   dlmwrite(nameFile, XSparse(:,i), '\n');         
    % end
    
    % dlmwrite('XS-512lenna-b32-o2-k20-m80',XS,';');
    % dlmwrite('XSparse-512lenna-b32-o2-k20-m80',XSparse,';');
    
    %xsparse = dlmread('xsparse-512lenna-b32-o2-k20-m80',';');
    %xs = dlmread('xs-512lenna-b32-o2-k20-m80',';');
    
    
    F = rearrangeBlocks(m, n, XS, over, blockSize, q);    
    figure('Name','Lenna');
    subplot(3,1,1); imshow(originalImage, [0 255]);
    subplot(3,1,2); imshow(F, [0 255]);   
   
    %% Second) Get measurments from the image with the image blocks
    
    %{ Take each block (column) in X and get its measurement vector y %}
    %{ Generate SBHE projection matrix of each block %}
    
    % sbhe = generatesbhe(blockSize, mes, 3);
    sbhe = dlmread('phi32-80sbhe4.out',';');
    
    % Build measurement matrix A
    A = sbhe*wt;

    % load('phi32-80sbhe4.out');
    % A = phi32_80sbhe4*wt;
    
    % normalize the matrix
    A=normc(A);
    
    % Generate measurement vectors of the blocks
    Y = A*XS;

    %% Third) Recover the image - Lapped block-based CS

    %{ Recover each block with its measurement vector y Recovery process:
    %s, approximation of the coefficients, t: time invested in this
    %approximation %}
   
    % Empty matrix for the recovered blocks
    R = zeros (size(X,1),size(X,2));

    fprintf('Total number of blocks: %i\n', size(Y,2));
    
    times = zeros(size(Y,2),1);
    
    for i=1:size(Y,2)
        %{ recover the block bi, in time ti; change k according to the
        %number of coefficients to recover in each block, k is the number
        %of significant coefficients %}
       
        fprintf('OMP, block number: %i\n',i);
        
        tic
        [bi,ti] = omp_alg(Y(:,i), A, k, 1e-5, k, 0);
        rec =  wt*bi;

        % save the ith recovered block in R
        R(:,i) = rec;
        times(i,1)=toc;
        
        %{ Uncomment the following 4 lines if you want to know the PSNR of each recovered block %}
        
        % xrec = reshape(rec,blockSize,blockSize);
        % xsparse = reshape (XS(:,i),blockSize,blockSize);
        % fprintf('\nPSNR OMP:\n');
        % psnr = fun_PSNR(xsparse,xrec)
        
    end
  
    % save the recovered blocks of the image    
    % dlmwrite('R512lenna-b32-k20-m80.mat',R,';');
        
    %% Four) Reconstruct the whole image using the recovered blocks, then take the PSNR of the reconstructed image
        
    Xsparse = rearrangeBlocks(m, n, XSparse, over, blockSize, q); % original sparse image
    Xrec =  rearrangeBlocks(m, n, R, over, blockSize, q);  % recovered image  
    
    psnr2 = fun_PSNR(Xsparse,Xrec);
        
    figure('Name','Lenna Recovery Results');
    subplot(2,1,1); imshow(Xsparse, [0 255]);
    title('Original Sparse Lenna 512x512');
    subplot(2,1,2); imshow(Xrec, [0 255]);
    title('Recovered Lenna 512x512 block size 32');
    
    % total time recovery blocks
    total_time = sum(times);
    
    
    
    
