% Compute a Sylvester Hadamard Matrix of 2^k order

function Sk = sylvester_matrix(k)

% H = [1 1; 1 -1];
H = [1 1; 1 0];

Sk = kron(H,H);

for i=1:1:k-2
    
    Sk= kron(H,Sk);

end
