README FILE
=============================================================

main file: compress_recover_BlockLappedCS.m

Este archivo muestra un ejemplo del enfoque de compresión y recuperación de una imagen por bloques sobrepuestos.

El ejemplo dado por defecto se da con una imagen de 512x512 pixeles pero puede ser configurado para las otras dimensiones de imágenes disponibles en la carpeta 'images'.

Los principales parámetros que se pueden configurar son los siguientes:

- La dimensión de la imagen: dada por el archivo de imagen cargado (variable dim).
- El tamaño de los bloques por los cuáles de va a dividir la imagen (variable blocksize -- número entero)
- El porcentaje de dispersión de la imagen (variable spar)

Algunas secciones del código muestran cómo los resultados pueden ser almacenados en disco para una posterior utilización.

Los principales algoritmos que forman parte del enfoque OBB-CS están en los siguientes archivos:

- lappedblock.m : Crea los bloques sobrepuestos de la imagen 
- rearrangeBlocks.m : Con los bloques reconstruidos este algoritmo reconstruye la imagen teniendo en cuenta las partes sobrepuestas entre los bloques.
- omp_alg.m : Algoritmo OMP, utilizado para hallar la reconstrucción de cada bloque de la imagen.
- {calculate_rp.m, compute_scramble_operator.m, generatesbhe.m, relative_prime.m, sylvester_matrix} : Funciones utilizadas en la generación de la matriz de muestreo SBHE.


Carpeta example_results:

Esta carpeta muestra algunos resultados obtenidos para una imagen 512x512

=============================================================
Contacto: irenelizeth@gmail.com