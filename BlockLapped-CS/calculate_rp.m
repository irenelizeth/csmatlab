% Compute a relative prime A with N

function A = calculate_rp(N)

A = 0;
 while A == 0
     % Generate a random number A
     rand(1);
     A = floor((N-1)*rand(1));
     % Check if the A number is relative prime with N
     if(~relative_prime(A,N))
         A=0;
     end
 end
 
%  imagesc(A);