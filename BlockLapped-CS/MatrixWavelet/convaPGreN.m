function M=convaPGreN(f,n)
f=(f);
p=length(f);
M=zeros(n,n);
for j=1:n
    F=zeros(1,(fix(p/n)+2)*n);
    F(j:j+p-1)=f;
    F=F(1:(fix(p/n)+2)*n);
    F=sum(reshape(F,[n (fix(p/n)+2)]),2)';
    M(j,:)=F;
end    