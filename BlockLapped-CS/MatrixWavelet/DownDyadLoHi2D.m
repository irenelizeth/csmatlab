function M=DownDyadLoHi2D(qmf,nc,nr)

D1=DownDyadLo2(qmf,nr);
D2=DownDyadHi2(qmf,nr);
D=[D1;D2];

M=[];
for j=1:nc
M=blkdiag(M,D);
end
