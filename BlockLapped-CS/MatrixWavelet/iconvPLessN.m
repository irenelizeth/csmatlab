function M=iconvPLessN(f,n)
f=(f);
p=length(f);
M=zeros(n,n);
for j=1:n
    F=zeros(1,(fix(p/n)+2)*n);
    if j<=p
        F(1:j)=f(j:-1:1);
        F(n-(p-1-j):n)=f(p:-1:j+1);       
    else
           F(j-p+1:j)=f(p:-1:1);
    end
    F=F(1:(fix(p/n)+2)*n);
    F=sum(reshape(F,[n (fix(p/n)+2)]),2)';
    %F=reverse(F);
    M(j,:)=F;
end    