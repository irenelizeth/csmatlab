function M=iconv2(f,n)
p=length(f);
if p>n
    M=iconvPGreN(f,n);
else
        M=iconvPLessN(f,n);

end