function Y=zerosM(qmf,nc0,nr0,J)
nc=nc0/2^(J-1);
nr=nr0/2^(J-1);
D2Hi=DownDyadHi2D(qmf,nc,nr);
D2Lo=DownDyadLo2D(qmf,nc,nr);
Dmat=[D2Lo;D2Hi];
y=DownDyadLoHi2D(qmf,nc,nr)*Dmat;
Y=eye(nc0*nr0,nr0*nc0);
if J>1
    contk=0;
    for k=1:2^(J-1):2^(J-1)*nc
        contk=contk+1;
        contr=0;
        for r=1:2^(J-1):2^(J-1)*nc
            contr=contr+1;
            Y(nr*(k-1)+1:nr*k,nr*(r-1)+1:nr*r)=y((contk-1)*nr+1:nr*contk,nr*(contr-1)+1:contr*nr);
        end
    end
else
    Y=y;
end
    
    
    
    
