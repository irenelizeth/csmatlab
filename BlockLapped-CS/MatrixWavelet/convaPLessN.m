function M=convaPLessN(f,n)
f=(f);
p=length(f);
M=zeros(n,n);
for j=1:n
    F=zeros(1,n);
    if (j+p-1)<=n
        F(j:j+p-1)=f;
    else
        F(j:n)=f(1:n-j+1);
        F(1:p-(n-j+1))=f(n-j+2:p);
    end
    M(j,:)=F;
end    