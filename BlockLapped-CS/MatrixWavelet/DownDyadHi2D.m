function M=DownDyadHi2D(qmf,nc,nr)

D=DownDyadHi2(qmf,nc);
M=[];
for j=1:nc/2
    rowD=zeros(1,nr*nc);
    rowD(1:nr:nr*nc)=D(j,:);
    
    for h=1:nr
        M=[M;circshift(rowD,[0 (h-1)])];
    end
end
 