clear;
close all;
clc;



I=double(rgb2gray(imread('lena.jpg')));
I=imresize(I,[32,32]);
%I(201:256,201:256)=0;
qmf=MakeONFilter('Symmlet',8);
F=FWT2_POE2(I,1,qmf);
IF=IWT2_POE2(F,2,qmf);

close all; imshow((abs(IF)),[]);
%close all; plot(sort(abs(F(:)),'descend'))
return

Iw=I;
S=4;
mode=2;
for j=1:S
[W,W1{j},W3{j},W2{j},W4{j}]=Wav2DH(Iw,mode);
[size(Iw)' size(W)'];
%close all;imshow(W,[])
%pause
Iw=W1{j};
end
Ir=W1{S};
for k=S:-1:1
    Ir=[ Ir W3{k};
        W2{k} W4{k}];
    Ir=InvWav2DH(Ir,mode);
end
W=W1;
for k=S:-1:1
    W=[W W3{k};
       W2{k} W4{k}];

end
close all; 
h=MakeONFilter('Symmlet',8);
imshow(log(abs(W)),[]);
Wr=FWT2_PO(I,4,h);
T1=sort(W(:),'descend');
T2=sort(Wr(:),'descend');
close all; subplot(2,1,1); plot(T1(1:500));
 subplot(2,1,2); plot(T2(1:500));
    
    