function index = getIndex(oldIndex, blockSize, overlap);

if oldIndex==1
    index=1;
elseif oldIndex==2
    index = blockSize+1;
else
    index = getIndex(oldIndex-1, blockSize, overlap) + blockSize-overlap;
end

