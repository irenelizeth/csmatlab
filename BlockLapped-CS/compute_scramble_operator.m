% Implementacion SBHE
% Parameters:
% N - Quadrature image dimension
% m - mode: 
%               1, if simple pixel level scrambler
%                   xb - P_sub_N is the scramble operator
%                   x - input signal
%               2, if Row Permutation and Row Inversion (RPRI) Scrambler
                   
function [P,A] = compute_scramble_operator(N,m)

if (m==1)
    
    % Scramble Operator - Linear Congruential Permutation (LCP)
    
    x = [1:N];
    xp = zeros(N,1);
    disp('A');
    
%   Verify the relatively number A with N
    A = calculate_rp(N)
%     A = 26773;
%       A = 15773
%     A = 39193;
    % A = 157;
    % A = 1001;
    for i=1:1:N
        pi = mod(A*(i-1),N) + 1;
        xp(i) = x(pi);
    end
    P=sparse(N,N);

    
    % Generating the matrix of the scramble operator
    for i=1:1:N 
        P(xp(i),i)=1; 
    end
    
elseif m==2
    
    % Scramble Operator - Row Permutation and Row Inversion (RPRI)  
    % Ramdomly permuted the rows of a 2D-image
    xp = randperm(256);
    xp = reshape( xp,256,1);
    
    P=sparse(N,N);
    
    % Generating the matrix of the scramble operator
    for i=1:1:N 
        P(xp(i),i)=1; 
    end
    
    % Reverse the even numbered rows
    for c=2:2:N
        %fliplr(A) returns a vector of the same length with the order of
        %its elements reversed
        P(c,:)=fliplr(P(c,:));
    end
end


