% Example using the recovery algorithms
close all;
clc
clear

% dimension fo the images
% dimv = [32, 48, 64];
dimv = [48];

% name of the images
image= 'xlen';
%image= 'xboat';

% Name of the Psi matrix type
%typePsi = 'gauss';
 typePsi = 'sbhe';

% sparsity levels for the images
% sparv = [0.05, 0.10, 0.15, 0.20];
sparv = [0.2];

% Measurements according to sparsity level (4:1)
%mesv = [20,40,60,80];
mesv = [80];

lambda = 0.1; 
niter = 500;

% nameFileIht =  strcat('resPsnr/iht-results-05-17-2012',image,'-',typePsi,'.txt');
% nameFileOmp =  strcat('resPsnr/omp-results-05-17-2012',image,'-',typePsi,'.txt');

% fid = fopen(nameFileIht, 'wt+');
% info = strcat('lambda',num2str(lambda),' | ','niter',num2str(niter),' | ',typePsi);
% fprintf(fid, '%s\r\n', info);
% info = strcat('NImage | Dim | k |  m | PSNR | tr  ;');
% fprintf(fid, '%s\r\n', info);
% fclose(fid);
% 
% fid = fopen(nameFileOmp, 'wt+');
% info = strcat('lambda',num2str(lambda),' | ','niter','--',' | ',typePsi);
% fprintf(fid, '%s\r\n', info);
% info = strcat('NImage | Dim | k |  m | PSNR | tr  ;');
% fprintf(fid, '%s\r\n', info);
% fclose(fid);
 
%% for each quantity of measurements
for l=1:1:size(mesv,2)
  mes = mesv(l); 
    %% For each dimension of the image
    for i=1:1:size(dimv,2)   

        dim = dimv(i);

        namImg = ['test/inter-img/',image,num2str(dim),'-inter.mat'];
        % recover the data of the interpolated image
        load(namImg);        
        %for boat x=x'
        %x=x';
        % side dim of the image
        n=sqrt(size(x,1));

        %Loading representation matrix, Psi
        namM = strcat('test/','w',num2str(dim),'.mat');
        load(namM);
        namMt= ['test/','wt',num2str(dim),'.mat'];
        load(namMt);

        %% For each sparsity level
        for j=1:1:size(sparv,2)

            spar = sparv(j);
            % set the sparsity level
            k = round(size(x,1)*spar);
           
            % set the number of elements to convert to zero (forcing sparsity)
            con = round((1-spar)*size(x,1));
           
            vecImg = w*x;
            vecImgSort = sortrows([abs(vecImg),(1:length(vecImg))'],1);
            %forcing k sparsity
            vecImg(vecImgSort(1:con,2))=0;
            % getting the new sparse image in the original domain
            vec = wt*vecImg;
            % sparse image in the original domain
            xsparse = reshape(vec, n,n);
            % showing the k-sparse image

                figure('Name',strcat('Sparse Image K',num2str(spar),' Dim',num2str(dim),'-M',num2str(mes)));
                subplot(3,1,1); imshow(xsparse, [0 255]);
                title('original sparse lena');

            % set the number of measurements
            % m = round(size(x)*(mes(it)/100));

            switch(typePsi)
                case 'gauss'
                    dir = 'test/gaussian/';
                case 'sbhe'
                    dir = 'test/sbhe/';
            end

            %saving the sparse image in a file into the test/ dir
            % namSparImg = strcat('res/',image,num2str(dim),'-k',num2str(spar),'-m',num2str(mes),'.out');
            % save(namSparImg,'xsparse');

            %Loading measurement matrix
            namePsiFile = strcat(dir,'phi',num2str(dim),'-',num2str(mes),typePsi,'.mat');
            load(namePsiFile);

            A = phi*wt;
            % normalize the image
            A=normc(A);

            %  Using a sparse version of the lena image
            y = A*vecImg;

            %Recovery process: s, approximation of the coefficients, t:
            %time invested in this approximation
           [s,t] = iht_alg(y,A,zeros(n^2,1),k,lambda,niter);
           [s2,t2] = omp_alg(y, A, k, 1e-5, k);

            rec =  wt*s;
            rec2 =  wt*s2;
            
            xrec = reshape(rec,n,n);
            xrec2 = reshape(rec2,n,n);

            % save recovered image
%             namRecImg = strcat('res/',image,num2str(dim),'-k',num2str(spar),'-m',num2str(mes),'-Psi',typePsi,'-recIHT.out');
            %save(namRecImg,'xrec');
            
%             namRecImg =
%             strcat('res/',image,num2str(dim),'-k',num2str(spar),'-m',num2str(mes),'-Psi',typePsi,'-recOMP.out');
%             save(namRecImg,'xrec2');

            fprintf('PSNR IHT:\n');
            psnr = fun_PSNR(xsparse,xrec);
            
            fprintf('\nPSNR OMP:\n');
            psnr2 = fun_PSNR(xsparse,xrec2);

                     
            subplot(3,1,2); imshow(xrec, [0 255]); 
            title('recovered  IHT');
            
            subplot(3,1,3); imshow(xrec2, [0 255]); 
            title('recovered  OMP');

            % open the file with write permission
%             info = strcat(image,' | ',num2str(dim),' | ',num2str(spar),' | ',num2str(mes),' | ',num2str(psnr),' | ',num2str(t),';');
%             fid=fopen(nameFileIht,'a+');
%             fprintf(fid, '%s\n',info);
%             fclose(fid);
%             
%             info = strcat(image,' | ',num2str(dim),' | ',num2str(spar),' | ',num2str(mes),' | ',num2str(psnr2),' | ',num2str(t2),';');
%             fid=fopen(nameFileOmp,'a+');
%             fprintf(fid, '%s\n',info);
%             fclose(fid);
%             
            str = strcat('image',image,' ',num2str(dim),' -> non-zero elements: ',num2str(k));
            disp(str);

        end
    end
end