function [x0,te] = iht_alg(y, A, x0, k, lambda, niter)
%Iterative Hard Thresholding (IHT) algorithm
% Input
%   Mandatory:
%               y   	 Observation vector to be decomposed (lengh: m)
%               A   	 Either:
%                       	1) An mxn matrix (m must be dimension of y)
%                       	2) A function handle 
%               n   	 length of x
%               lambda   threshold to be used


% max number of iterations
n=size(A,2);
At = A';
% residual
err = 1e-5;

fprintf('IHT\n');
    
tic

for i=0:niter
    
    v = y - A*x0;
    
%         if i==(niter-1)
         max(abs(v));
%         end
    a= x0 + lambda*At*v;
    % residual
    %sort the elements of the vector in ascending order
    [~,id] = sort(abs(a),'descend');
    %select the m higher values, update the values of the vector x
    x0=zeros(n,1);
    x0((id(1:k))) = a(id(1:k));
    
    normV = norm(v);
   
    if(normV < err)
        break;
    end
    
    %fprintf('Iteration %u, l2-v: %f \n',i,normV);
    
end
te = toc