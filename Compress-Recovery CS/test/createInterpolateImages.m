clc;
image= 'xboat';
dimv = [32, 48];

for i=1:1:size(dimv,2)
    
    dim = dimv(i);

    namImg = ['img/',image,num2str(dim),'.out'];

    % recover the data of the interpolated image
    I = load(namImg);
    % scale the interpolated image
    x = (double((I-(min(I)))./(max(I)-min(I))*255))';
    % side dim of the image
    n=sqrt(size(x,1));
    
    namImg = strcat(image,num2str(dim),'-inter.out');
    namImgMat = strcat('xboat',num2str(dim),'-inter.mat');
    
    dlmwrite(namImg,x,';');
    save(namImgMat,'x');
    
end